//试卷列表插件
function  paper() {
    // 定义一个名为 pro-fessional 的新组件
    var html = "<li v-if='type==1' class='paper' :data-id='id' :data-questionId='questionid' :data-sort='sort'>"
                    +"<div class='paper_title'>{{index+1}}.&nbsp&nbsp{{questionname}}[{{score}}分]</div>"
                    +"<div class='paper_list'>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img1'></span>&nbsp&nbsp<span class='cur_sel' data-val='A'>A</span>&nbsp&nbsp{{answer1}}</div>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img2'></span>&nbsp&nbsp<span class='cur_sel' data-val='B'>B</span>&nbsp&nbsp{{answer2}}</div>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img3'></span>&nbsp&nbsp<span class='cur_sel' data-val='C'>C</span>&nbsp&nbsp{{answer3}}</div>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img4'></span>&nbsp&nbsp<span class='cur_sel' data-val='D'>D</span>&nbsp&nbsp{{answer4}}</div>"
                    +"</div>"
                    +"<div class='paper_answer'>我的答案&nbsp<span class='redColor' :data-val='studentanswer'>{{stuAns}}</span></div>"
                +"</li>"
                +"<li v-else-if='type==2' class='paper' :data-id='id' :data-questionId='questionid' :data-sort='sort'>"
                    +"<div class='paper_title'>{{index+1}}.&nbsp&nbsp{{questionname}}[{{score}}分]</div>"
                    +"<div class='paper_list'>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img1'></span>&nbsp&nbsp<span class='cur_sel' data-val='A'>{{answer1}}</span></div>"
                        +"<div class='paper_eve'><span class='paper_eveSel' @click='sel($event)'><img :src='img2'></span>&nbsp&nbsp<span class='cur_sel' data-val='B'>{{answer2}}</span></div>"
                    +"</div>"
                    +"<div class='paper_answer'>我的答案&nbsp<span class='redColor' :data-val='studentanswer'>{{stuAns}}</span></div>"
                +"</li>";
    var paperList =  Vue.component('pub-course', {
        props: ['id','questionid','index','questionname','score','answer1','answer2','answer3','answer4','type','sort','studentanswer'],
        data: function () {
            return {
                img1:"./img/sel01.png",
                img2:"./img/sel01.png",
                img3:"./img/sel01.png",
                img4:"./img/sel01.png"
            }
        },
        mounted:function(){
            this.answerInit();
            
        },
        methods:{
            //点击选择
            sel:function (that) {
                var curText = "";//当前选中的答案文本
                var curVal = "";//当前选中的答案值
                if($(that.currentTarget).find("img").attr("src")=="./img/sel01.png"){
                    $(that.currentTarget).parents(".paper_list").find("img").attr("src","./img/sel01.png");
                    $(that.currentTarget).find("img").attr("src","./img/sel02.png");
                    curText =$(that.currentTarget).parent().find(".cur_sel").text();
                    curVal =$(that.currentTarget).parent().find(".cur_sel").attr("data-val");
                }else{
                    $(that.currentTarget).parents(".paper_list").find("img").attr("src","./img/sel01.png");
                    curText = "";
                    curVal = "";
                }
                $(that.currentTarget).parents(".paper").find(".redColor").text(curText);
                $(that.currentTarget).parents(".paper").find(".redColor").attr("data-val",curVal);
            },
            //初始化答案
            answerInit:function(){
                this.img1="./img/sel01.png";
                this.img2="./img/sel01.png";
                this.img3="./img/sel01.png";
                this.img4="./img/sel01.png";
                if(this.studentanswer=="A"){
                    this.img1="./img/sel02.png";
                }else if(this.studentanswer=="B"){
                    this.img2="./img/sel02.png";
                }else if(this.studentanswer=="C"){
                    this.img3="./img/sel02.png";
                }else if(this.studentanswer=="D"){
                    this.img4="./img/sel02.png";
                }
            }
        },
        computed:{
            stuAns:function(){
                
                if(this.type==2){
                    var value = this.studentanswer;
                    if(value=="A"){
                        value="是";
                    }else if(value=="B"){
                        value="否";
                    }
                    return value;
                }else{
                    return this.studentanswer;
                }
                
            }
        },
        template: html
    });
    return paperList;
}
