//首页专业课程列表插件
function  proFessional() {
    // 定义一个名为 pro-fessional 的新组件
    var html = "<div class='professEve' v-if='subjectid' @click='disDetail(subjectid,$event)'>"//判断是否是考试中心的列表
            +"<div class=professEve_img><img class=imgBai :src='add(photo)'/></div>"
            +"<div class=professEve_font>"
                +"<div class=professEve_bfont>{{name}}</div>"
                +"<div class=professEve_sfont>{{introduce}}"
                    +"<span v-if='tptype'>"//判断是否考试中心列表
                        +"<span v-if='tptype==1'><button class='blueBtn'>继续学习</button></span>"
                        +"<span v-else-if='tptype==2'><button class='blueBtn' @click.stop='disPaper(subjectid,$event)'>开始考试</button></span>"
                        +"<span v-else-if='tptype==3'><button class='blueBtn' @click.stop='disPaper(subjectid,$event)'>继续考试</button></span>"
                        +"<span v-else-if='score' class='professEve_score'>{{score}}分</span>"
                    +"</span>"
                +"</div>"
            +"</div>"
        +"</div>"
        +"<div class='professEve' v-else @click='disDetail(id,$event)'>"
            +"<div class=professEve_img><img class=imgBai :src='add(photo)'/></div>"
            +"<div class=professEve_font>"
                +"<div class=professEve_bfont>{{name}}</div>"
                +"<div class=professEve_sfont>{{introduce}}"
                    +"<span v-if='tptype'>"//判断是否考试中心列表
                        +"<span v-if='tptype==1'><button class='blueBtn'>继续学习</button></span>"
                        +"<span v-else-if='tptype==2'><button class='blueBtn' @click.stop='disPaper(subjectid,$event)'>开始考试</button></span>"
                        +"<span v-else-if='tptype==3'><button class='blueBtn' @click.stop='disPaper(subjectid,$event)'>继续考试</button></span>"
                        +"<span v-else-if='score' class='professEve_score'>{{score}}分</span>"
                    +"</span>"
                +"</div>"
            +"</div>"
        +"</div>";
    var professional =  Vue.component('pro-fessional', {
        props: ['index','id','name','introduce','photo','score','tptype','stutype','subjectid'],
        data: function () {
            return {
                title:""
            }
        },
        methods:{
            //跳转到课程详情
            disDetail:function (id,that) {
                var level1 = sessionStorage.getItem("level1");
                var level2 = $(".videoTop .videoTop_tar").text();
                var level3 = $(that.currentTarget).find(".professEve_bfont").text();
                var crumbs = JSON.stringify({level1:level1,level2:level2,level3:level3});
                sessionStorage.setItem("crumbs",crumbs);
                window.open("./courseDetail.html?id="+id);
                // window.location.href="./courseDetail.html?id="+id;
            },
            //跳转到试卷页面
            disPaper:function(id){
                window.open("./examPaper.html?id="+id);
                // window.location.href="./examPaper.html?id="+id;
            },
            add:function (url) {
                return "/content"+url;
            }
        },
        template: html
    });
    return professional;
}