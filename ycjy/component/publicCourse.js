//首页公共课程列表插件
function  pubCourse() {
    // 定义一个名为 pro-fessional 的新组件
    var html = "<div class=centerCon @click='disDetail(id,$event)'>"
        +"<div class=centerCon_img><img class=imgBai :src='add(image)'\></div>"
        +"<div class=centerCon_font>"
        +"<div class=centerCon_bfont>{{name}}</div>"
        // +"<div class=centerCon_sfont>{{introduce}}</div>"
        +"</div>"
        +"</div>";
    var pubCourse =  Vue.component('pub-course', {
        props: ['id','name','image'],
        data: function () {
            return {
                title:""
            }
        },
        methods:{
            //跳转到视频课程
            disDetail:function (id,that) {
                sessionStorage.setItem("level1",$(that.currentTarget).find(".centerCon_bfont").text());
                window.location.href="./video.html?id="+id;
            },
            add:function (url) {
                return "/content"+url;
            }
        },
        template: html
    });
    return pubCourse;
}
