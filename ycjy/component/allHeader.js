function  allHeader() {
    // 定义一个名为 allFooter 的新组件
    var html = "<header>"
            +"<div class='centerTop'>"
                +"<div class='centerLogo left' @click='disIndex'>"
                    +"<span><img src='./img/logo.png'></span>"
                    +"<span class='centerLogo_text'>三河职教非全日制远程教育系统</span>"
                +"</div>"
                +"<div class='centerList'>"
                    +"<div @click='disIndex'>首页</div>"
                    +"<div v-if='courseDetail' @click='disExam'>考试中心</div>"
                    // +"<div @click='disVideo'>视频课程</div>"
                +"</div>"
                +"<div class='centerLr' v-if='loginInfo'>"
                    +"<span @click='disCenter'>"
                    +"<div class='centerLr_div'>{{loginInfo.info.name}}</div></span>"
                    +"<div class='centerLr_div' @click='exit'>退出</div>"
                +"</div>"
                +"<div class='centerLr' v-else>"
                    +"<div class='centerLr_div' @click='disCenter'>个人中心</div>"
                +"</div>"

            +"</div>"
            +"</header>";
    var allheader =  Vue.component('all-header', {
        data: function () {
            return {
                loginInfo:"",
                courseDetail:""
            }
        },
        mounted:function(){
            this.isLogin();
            this.isPage();
        },
        methods:{
            //判断是否有登录
            isLogin:function(){
                var loginInfo = sessionStorage.getItem("loginInfo");
                if(loginInfo != null){
                    this.loginInfo = JSON.parse(loginInfo);
                }
                // if($("#mobile").attr("id")=="mobile"){
                //
                // }else{
                //     var loginInfo = sessionStorage.getItem("loginInfo");
                //     this.loginInfo = JSON.parse(loginInfo);
                // }
            },
            //判断当前在哪个页面
            isPage:function(){
                if($("#courseDetail").attr("id")=="courseDetail"||$("#video").attr("id")=="video"){
                    this.courseDetail = true;
                    $(".centerList > div").eq(1).addClass("centerList_tar").siblings().removeClass("centerList_tar");
                }else{
                    this.courseDetail = "";
                }
                // else if($("#exam").attr("id")=="exam"){
                //     $(".centerList > div").eq(1).addClass("centerList_tar").siblings().removeClass("centerList_tar");
                // }
            },
            //退出按钮
            exit:function(){
                sessionStorage.clear();
                window.location.href = "http://192.168.2.66/school-cas/logout?service=http://192.168.2.66/school-cas?service="+encodeURI("http://192.168.2.70:8082./index.html");
            },
            //跳转到首页按钮
            disIndex:function () {
                window.location.href='./index.html';
            },
            //跳转到视频课程按钮
            disVideo:function () {
                window.location.href='./video.html';
            },
            //跳转到考试中心按钮
            disExam:function () {
                window.location.href='./exam.html';
            },
            //跳转到个人中心按钮
            disCenter:function () {
                window.location.href='./center.html';
            }
        },
        template: html
    });
    return allheader;
}