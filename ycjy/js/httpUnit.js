
//var host="";
var host="http://192.168.2.70:8082";
// var host="http://192.168.0.104:8082";

function httpHelperget(url, callback){


    url = host+url;

    var token = "";

    var loginInfo = sessionStorage.getItem("loginInfo");
    if (loginInfo == null||loginInfo==undefined||loginInfo=="") {
        
    }else{
        var value = JSON.parse(loginInfo);
        token ="Bearer "+ value.jwt;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");
    xhr.setRequestHeader("Content-Type","application/json;charset=UTF-8");  
    xhr.setRequestHeader("Authorization", token);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
            var httpStatus = xhr.responseText;
            callback(true, xhr);
        }else if (xhr.readyState == 4 && xhr.status == 226){
        	
        }else{
        	
        }
    };
    xhr.send();

};

//post
function httpHelperpost(url,params,callback){  

    url = host+url;

    var token = "";
    var loginInfo = sessionStorage.getItem("loginInfo");
    if (loginInfo == null||loginInfo==undefined||loginInfo=="") {
        
    }else{
        var value = JSON.parse(loginInfo);
        token ="Bearer "+ value.jwt;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", url);  
    xhr.setRequestHeader("Content-Type","application/json;charset=UTF-8");  
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");
    xhr.setRequestHeader("Authorization", token);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
            var httpStatus = xhr.responseText;
            callback(true, xhr);
        }else if (xhr.readyState == 4 && xhr.status == 226){
        }else{
        }
    };
    xhr.send(params);
}  



    //post
 function httpHelperput(url,params,callback){  

    url = host+url;


    var token = "";
    var loginInfo = sessionStorage.getItem("loginInfo");
    if (loginInfo == null||loginInfo==undefined||loginInfo=="") {
        
    }else{
        var value = JSON.parse(loginInfo);
        token ="Bearer "+ value.jwt;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("PUT", url);  
    xhr.setRequestHeader("Content-Type","application/json;charset=UTF-8");  
    xhr.setRequestHeader("Access-Control-Allow-Origin","*");
    xhr.setRequestHeader("Authorization", token);
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
            var httpStatus = xhr.responseText;
            callback(true, xhr);
        }else if (xhr.readyState == 4 && xhr.status == 226){
        }else if (xhr.readyState == 4 && xhr.status == 417){
        }else{
        }
    };
    xhr.send(params);

    }  

 //    httpHelperpost("http://127.0.0.1:3000/test","id=1&ids=2", function(err,data){  
 // if(err){  
 // //错误处理  
 // }else{  
 // cc.log(data);  
 // }  
 // }) 